# Assignment 3 - Sign Language Translator


## Deployment:
Deployed on Heroku

[Nicole's](https://frozen-cove-24939.herokuapp.com/) - URL:  <https://frozen-cove-24939.herokuapp.com/>

[Camilla's](https://nameless-ravine-28050.herokuapp.com/) - URL:  <https://nameless-ravine-28050.herokuapp.com/>

## Source:

<https://gitlab.com/nicben/translation-assignment/>


## Component Tree

Link to [component tree](https://gitlab.com/nicben/translation-assignment/-/blob/master/assets/Component-tree.pdf)


