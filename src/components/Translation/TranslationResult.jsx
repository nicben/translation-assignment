import {useEffect, useState} from "react";


const TranslationResult = ({translateValue}) => {

    const [images, setImages] = useState([])

    translateValue = translateValue.replace(/[^a-z]/g, "")

    useEffect(() => {
        if (translateValue) {
            const inputArray = [...translateValue]
            const inputImg = inputArray.map((char, index) => {
                if(char !== " ") {
                    return <img key={index} src={'/individual_signs/' + char + '.png'} alt={"sign"} height={70}/>
                }
                else {
                    return <img key={index} src={'/individual_signs/blank.png'} alt={"sign"} height={70}/>
                }
            })
            setImages(inputImg)
        }
    }, [translateValue]);


    return (
        <>
            <div> {images} </div>
        </>
    );
};

export default TranslationResult;