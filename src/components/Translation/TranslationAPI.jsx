//const apiNicole = 'https://trivia-assignment-api.herokuapp.com/'
const apiCamilla = 'https://noroff-assignment-test-api.herokuapp.com'
const apiKey = "token";

export const TranslationAPI = {
    addTranslation(userId, translation) {
        const requestOptions = {
            method: "PATCH",
            headers: {
                "X-API-Key": apiKey,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                translations: translation
            })
        };
        return fetch(`${apiCamilla}/translations/${userId}`, requestOptions);
    }
}