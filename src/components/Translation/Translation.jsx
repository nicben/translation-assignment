import {useEffect, useState} from "react";
import AppContainer from "../../hoc/AppContainer";
import CardContainer from "../CardContainer/CardContainer";
import withUser from "../../hoc/withUser";
import {useTranslationContext} from "../../context/TranslationContext";
import {TranslationAPI} from "./TranslationAPI";
import TranslationResult from "./TranslationResult";
import {useUserContext} from "../../context/UserContext";
import OrangeContainer from "../OrangeContainer/OrangeContainer";
import InputField from "../InputField/InputField";
import styles from './Translation.module.css'


const Translation = () => {

    console.log('Translation.render')

    const [translate, setTranslate] = useState('');
    const [input, setInput] = useState('');
    const {userState} = useUserContext();
    const {translationState, translationDispatch} = useTranslationContext();
    const userId = userState.user.id

    const handleInputChange = e => {
        setInput(e.currentTarget.value)
    }

    const handleTranslationSubmit = e => {
        if(input.length > 0) {
            e.preventDefault()
            setInput('')
            setTranslate(input)
            translationDispatch({type: 'ADD_TRANSLATION', payload: input.trim()})
        }
    }

    useEffect(() => {
        try {
            TranslationAPI.addTranslation(userId, translationState.translations)
        } catch (error) {
            translationDispatch({type: 'SET_TRANSLATION_ERROR', payload: error.message})
        }
    }, [translationState.translations]);

    return (
        <>
            <OrangeContainer style={{height: '11rem'}}>
                <AppContainer>
                    <InputField
                        id="translate_field"
                        value={input}
                        onChange={handleInputChange}
                        onClick={handleTranslationSubmit}
                        placeholder="Enter words"
                        className={styles.InputField}
                        keyboardStyle={styles.KeyboardIcon}
                        pipeIcon={styles.PipeIcon}
                        btnStyle={styles.Button}
                    />
                </AppContainer>
            </OrangeContainer>
            <AppContainer>
                <CardContainer label={true} className={styles.CardStyle} style={{height: "25rem"}}>
                    <TranslationResult translateValue={translate}/>
                </CardContainer>
            </AppContainer>
        </>
    );
};

export default withUser(Translation);