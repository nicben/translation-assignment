import styles from './CardContainer.module.css'

const CardContainer = ({ children, style, className, label}) => {

    return (
        <div className={`${styles.card} ${className}`}>
            <div className={ styles.body } style={style}>
                {children}
            </div>
            <div className={styles.footer}>
                {label &&
                <div className={styles.label}>
                    Translation
                </div>
                }
            </div>
        </div>
    );
};

export default CardContainer;