import styles from './OrangeContainer.module.css'

const OrangeContainer = ({ style, children }) => {
    return (
        <div className={styles.Background} style={style}>{children}</div>
    )
}
export default OrangeContainer