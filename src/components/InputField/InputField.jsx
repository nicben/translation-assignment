import styles from './InputField.module.css'

const InputField = ({ id, value, onChange, onClick, className, style, btnStyle, placeholder, keyboardStyle,pipeIcon }) => {


    return (
        <div className={styles.IconsIn}>
            <span className={`material-icons-outlined ${styles.KeyboardIcon} ${keyboardStyle}`}>keyboard_alt </span>
            <span className={`${styles.PipeIcon} ${pipeIcon}`}>|</span>
            <input
                type="text"
                id={id}
                value={value}
                required
                className={`${className} ${styles.InputField}`}
                style={style}
                onChange={onChange}
                placeholder={placeholder} />
            <button className={`${styles.Button} ${btnStyle}`} type="submit" onClick={onClick} >
                <span className={`material-icons-outlined ${styles.Arrow}`}>arrow_forward</span>
            </button>
        </div>
    )
}
export default InputField

