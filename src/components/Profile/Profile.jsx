import {useTranslationContext} from "../../context/TranslationContext";
import AppContainer from "../../hoc/AppContainer";
import CardContainer from "../CardContainer/CardContainer";
import TranslationList from "./TranslationList";
import withUser from "../../hoc/withUser";
import {useHistory} from "react-router-dom";
import {useUserContext} from "../../context/UserContext";
import {TranslationAPI} from "../Translation/TranslationAPI";
import OrangeContainer from "../OrangeContainer/OrangeContainer";
import styles from './Profile.module.css'

const Profile = () => {

    console.log('Profile.render')

    const {translationState, translationDispatch} = useTranslationContext();
    const {userState, userDispatch} = useUserContext()
    const history = useHistory()
    const userId = userState.user.id

    const handleClearClicked = () => {
        translationDispatch({type: 'CLEAR_TRANSLATION_LIST'})
        try {
            TranslationAPI.addTranslation(userId, [])
        } catch (error) {
            translationDispatch({type: 'SET_TRANSLATION_ERROR', payload: error.message})
        }
    }

    const handleLogoutClicked = () => {
        localStorage.setItem("username", '');
        userDispatch({type: 'REMOVE_USER'})
        translationDispatch({type: 'CLEAR_TRANSLATION_LIST'})
        userDispatch({type: 'SET_USER_ERROR', payload:""})
        translationDispatch({type: 'SET_TRANSLATION_ERROR',  payload:""})
        history.push('/')
    }

    return (
        <>
        <OrangeContainer style={{height: '11rem'}}>
            <p className={styles.MainTitle}>Your last 10 translations</p>
        </OrangeContainer>
        <AppContainer>

            <div style={{marginTop: "4.5rem"}}>
                <CardContainer label={false} style={{height: "25rem"}} className={styles.CardStyle}>
                    <TranslationList className={`${styles.ListStyle}`} translationList={translationState.translations}/>

                </CardContainer>
                <button className={`${styles.ButtonStyle} ${styles.LogOutButton}`} onClick={handleLogoutClicked}>
                    Logout
                </button>
                <button className={`${styles.ButtonStyle} btn-danger`}  onClick={handleClearClicked}>
                    Clear list
                </button>
            </div>
        </AppContainer>
        </>
    );
};

export default withUser(Profile);