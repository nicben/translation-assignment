const TranslationList = ({translationList, className}) => {

    return (
        <div className={className}>
            {translationList.length > 0 &&
            <ul style={{listStyleType: 'circle'}}>
                {translationList.slice(0, 10).map((translation, index) => <li key={index}>{translation}</li>)}
            </ul>
            }
        </div>
    );
};

export default TranslationList;