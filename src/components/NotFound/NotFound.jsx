import {Link, useHistory} from 'react-router-dom'
import AppContainer from "../../hoc/AppContainer";
import OrangeContainer from "../OrangeContainer/OrangeContainer";
import styles from "../NotFound/NotFound.module.css";


const NotFound = () => {
    const history = useHistory()

    const handleOnClick = () => {
        history.push('/')
    }
    return (
        <>
            <div style={{position: 'absolute'}}>
                <OrangeContainer style={{height:'25rem'}}>
                    <div>
                        <div className={`${styles.Image}`}>
                            <span><img
                                alt=""
                                src={'/Logo-Hello.png'}
                                className={styles.LogoStyle}
                            /></span>
                            <span><img
                                alt=""
                                src={'/Splash.png'}
                                className={styles.SplashStyle}
                            /></span>
                        </div>
                        <div className={`${styles.Title}`}>
                            <p className={styles.MainTitle}>Page Not Found</p>
                            <p className={styles.SubTitle}>The page you are looking for does not exist!</p>
                        </div>
                    </div>
                </OrangeContainer>
            </div>
            <AppContainer>
                <div>
                    <button className={`${styles.ButtonStyle} ${styles.LogOutButton}`} onClick={handleOnClick}>Take me Home</button>
                </div>
            </AppContainer>
        </>

    )
}
export default NotFound