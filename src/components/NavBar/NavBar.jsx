import { Navbar, Container } from 'react-bootstrap';
import {NavLink} from "react-router-dom";
import {useUserContext} from "../../context/UserContext";
import styles from './Navbar.module.css'

const NavBar = () => {
    console.log('NavBar.render')
    const { userState } = useUserContext()

    return (
        <>
            <Navbar className={styles.NavMenu}>
                <Container>
                    <Navbar.Brand className={styles.NavElement}>
                        {userState.user !=='' &&
                        <li className={styles.NavElement}>
                            <span><img
                            alt=""
                            src={'/Logo.png'}
                            className={styles.LogoStyle}
                            /></span>
                            <span><img
                                alt=""
                                src={'/Splash.png'}
                                className={styles.SplashStyle}
                            /></span>
                        </li>
                        }{' '}
                        <li className={styles.NavElement}>Lost in translation</li>
                    </Navbar.Brand>
                    {userState.user !=='' &&
                    <NavLink to={"/profile"}>
                        <span className={styles.Profile}> {userState.user.username}</span>
                        <span className= {`${styles.IconStyle} material-icons-outlined`}>account_circle</span>
                    </NavLink>
                    }
                </Container>
            </Navbar>
        </>
    )
}

export default NavBar