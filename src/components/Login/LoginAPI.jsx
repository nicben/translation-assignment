//const apiNicole = 'https://trivia-assignment-api.herokuapp.com/'
const apiCamilla = 'https://noroff-assignment-test-api.herokuapp.com'
const apiKey = "token";

export const LoginAPI = {
    async login(username) {
        try {
            const [user] = await fetch(`${apiCamilla}/translations?username=${username}`)
                .then(response => response.json());
            return [null, user]
        } catch (error) {
            console.log("loginAPI " + error.message)
            return [error.message, null]
        }
    },


    async registerUser(username) {
        const requestOptions = {
            method: "POST",
            headers: {
                "X-API-Key": apiKey,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                username: username,
                translations: []
            })
        };
        return fetch(`${apiCamilla}/translations`, requestOptions)
            .then(async (response) => await response.json())
    },
}