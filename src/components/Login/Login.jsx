import AppContainer from "../../hoc/AppContainer";
import {useUserContext} from "../../context/UserContext";
import {Redirect, useHistory} from "react-router-dom";
import {useEffect, useState} from "react";
import {LoginAPI} from "./LoginAPI";
import {useTranslationContext} from "../../context/TranslationContext";
import styles from './Login.module.css'
import OrangeContainer from "../OrangeContainer/OrangeContainer";
import InputField from "../InputField/InputField";
import CardContainer from "../CardContainer/CardContainer";

const Login = () => {

    console.log('Login.render')

    const [username, setUsername] = useState('')
    const [isLoggedIn, SetIsLoggedIn] = useState(false)
    const {userDispatch} = useUserContext()
    const {translationDispatch} = useTranslationContext();
    const history = useHistory()
    let localUsername = localStorage.getItem("username")

    useEffect(() => {
        const getLoggedInUser =  async () => {
            if(localUsername !== '') {
                try {
                    let [error, user] = await LoginAPI.login(localUsername)
                    if (user === undefined) {
                        userDispatch({type: 'SET_USER_ERROR', payload: error})
                    }
                    else {
                        userDispatch({type: 'SET_USER', payload: user})
                        translationDispatch({type: 'SET_TRANSLATIONS', payload: user.translations})
                        SetIsLoggedIn(user)
                    }

                    //SetIsLoggedIn(await login())
                }
                catch (e) {
                    userDispatch({ type: 'SET_USER_ERROR', payload: e.message })
                }
            }
        }

        getLoggedInUser()
    }, []);

    if(isLoggedIn) {
        return <Redirect to="/translation"/>
    }

    const onInputChange = (e) => {
        setUsername(e.currentTarget.value.toLowerCase())
    }

    const handleLoginClick = async () => {
        if(username.length > 0) {
            if (!await login()) {
                await registerUser()
            }
            if (userDispatch.error === undefined) {
                localStorage.setItem("username", "" + username);
                history.push('/translation')
            }
        }
    }

    const login = async () => {
        let [error, user] = await LoginAPI.login(username)
        if (user === undefined) {
            userDispatch({type: 'SET_USER_ERROR', payload: error})
            return false
        } else {
            userDispatch({type: 'SET_USER', payload: user})
            translationDispatch({type: 'SET_TRANSLATIONS', payload: user.translations})
            localUsername = user.username
            return true
        }
    }

    const registerUser = async () => {
        try {
            const user = await LoginAPI.registerUser(username)
            userDispatch({type: 'SET_USER', payload: user})
            localUsername = user.username
        } catch (error) {
            userDispatch({type: 'SET_USER_ERROR', payload: error.message})
        }
    }

    const inputStyle = {
        width: '57vw'
    }

    return(
        <div style={{position: 'absolute'}}>
            <OrangeContainer style={{height:'25rem'}}>
                    <div>
                        <div className={`${styles.Image}`}>
                            <span><img
                                alt=""
                                src={'/Logo.png'}
                                className={styles.LogoStyle}
                            /></span>
                            <span><img
                                alt=""
                                src={'/Splash.png'}
                                className={styles.SplashStyle}
                            /></span>
                        </div>
                        <div className={`${styles.Title}`}>
                            <p className={styles.MainTitle}>Lost in translation</p>
                            <p className={styles.SubTitle}>Get started</p>
                        </div>
                    </div>
            </OrangeContainer>
            <AppContainer >
                <div >
                    <CardContainer label={false} className={styles.CardStyle} >
                        <InputField
                            id={"username"}
                            placeholder={"What's your name?"}
                            onChange={onInputChange}
                            onClick={handleLoginClick}
                            style={inputStyle}
                            btnStyle={styles.ButtonStyle}
                        />
                    </CardContainer>
                </div>
            </AppContainer>
        </div>
    )
}
export default Login;