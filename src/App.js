import './App.css';
import {
    BrowserRouter,
    Switch,
    Route,
    //Redirect
} from 'react-router-dom'
import Login from './components/Login/Login'
import NotFound from './components/NotFound/NotFound'
import NavBar from "./components/NavBar/NavBar";
import Profile from "./components/Profile/Profile";
import Translation from "./components/Translation/Translation";

function App() {
    console.log('App.render')
    return (
        <BrowserRouter>
            <div className="App">
                <NavBar/>
                <Switch>
                    <Route path="/" exact component={Login}/>
                    <Route path="/translation" component={Translation}/>
                    <Route path="/profile" component={Profile}/>
                    <Route path="*" component={NotFound}/>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
