import {createContext, useContext, useReducer} from "react";

const TranslationContext = createContext('')

export const useTranslationContext = () => {
    return useContext(TranslationContext)
}

const translationReducer = (state, action) => {
    switch (action.type) {
        case 'SET_TRANSLATIONS':
            return {
                ...state,
                translations: [...action.payload, ...state.translations],
                error: ''
            }
        case 'ADD_TRANSLATION':
            return {
                ...state,
                translations: [action.payload, ...state.translations],
                error: ''
            }
        case 'CLEAR_TRANSLATION_LIST':
            return {
                translations: [],
                error: ''
            }
        case 'SET_TRANSLATION_ERROR':
            return {
                ...state,
                error: action.payload
            }
        default:
            return state
    }
}

const initialState = {
    translations: [],
    error: ''
}

const TranslationProvider = ({children}) => {

    const [translationState, translationDispatch] = useReducer(translationReducer, initialState)

    return (
        <TranslationContext.Provider value={{translationState, translationDispatch}}>
            {children}
        </TranslationContext.Provider>
    )
}

export default TranslationProvider