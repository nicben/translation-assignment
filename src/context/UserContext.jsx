import {createContext, useContext, useReducer} from "react";

const UserContext = createContext('')

export const useUserContext = () => {
    return useContext(UserContext)
}

const userReducer = (state, action) => {
    switch (action.type) {
        case 'SET_USER':
            return {
                error: '',
                user: action.payload
            }
        case 'SET_USER_ERROR':
            return {
                ...state,
                error: action.payload
            }
        case 'REMOVE_USER':
            return {
                error: '',
                user: ''
            }
        default:
            return state
    }
}

const initialState = {
    error: '',
    user: ''
}

const UserProvider = ({children}) => {

    const [userState, userDispatch] = useReducer(userReducer, initialState)

    return (
        <UserContext.Provider value={{userState, userDispatch}}>
            {children}
        </UserContext.Provider>
    )
}

export default UserProvider