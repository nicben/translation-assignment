import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import AppContext from "./context/AppContext";
import 'bootstrap/dist/css/bootstrap.min.css'
import './index.css';
ReactDOM.render(
    <React.StrictMode>
        <AppContext>
            <App />
        </AppContext>
    </React.StrictMode>,
    document.getElementById('root')
);

