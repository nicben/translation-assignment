import {Redirect} from "react-router-dom";

const withUser = Component => props => {
    const localUsername = localStorage.getItem("username")
    if (localUsername !== '') {
        console.log("translate" + localUsername)
        return <Component {...props} />
    } else {
        console.log("redirect"+ localUsername)
        return <Redirect to="/"/>
    }
}

export default withUser
